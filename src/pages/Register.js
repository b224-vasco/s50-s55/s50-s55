import { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){

	const { user, setUser } = useContext(UserContext);

	const [firstname, setfirstname] = useState('');
	const [lastname, setlastname] = useState('');

	const [email, setEmail] = useState('');

	const [mobileNo, setmobileNo] = useState('');

	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determing whe
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);


	// Function to simulate user registration
	function registerUser(e){
		console.log("here")
		e.preventDefault()

		// Clear the input fields and states
		setEmail("");
		setPassword1("");
		setPassword2("");

		/*fetch(`http://${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstname: firstname,
				lastname: lastname,
				email: email,
				mobileNo: mobileNo,
				password1: password1,
				password2: password2

			})
		})
		.then(res => res.json())
		.then(data => {
			// We will receive either a token or a false response.
			console.log(data)

			if(typeof data.access !== "undefined") {
				localStorage.setItem('token', data.access)

				Swal.fire({
					title: "Registration Successful",
					icon: "success",
					text: "Welcome to Zuitt!"
				})

			} else {

				Swal.fire({
					title: "Registration Failed",
					icon: "error",
					text: "Please, check your registration credentials and try again."
				})
			}
		})*/

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
				
			})
		})
		.then(res => res.json())
		.then(data => {
		
			console.log(data)

			if(data === true) {

				Swal.fire({
					title: "Duplicate email found!",
					icon: "error",
					text: "Please use a different email"
				})

			} else {

				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstname: firstname,
						lastname: lastname,
						email: email,
						mobileNo: mobileNo,
						password1: password1,
						// password2: password2

					})
				})
				.then(res => res.json())
				.then(data => {
					// We will receive either a token or a false response.
					console.log(data)

					if(data === true ) {
						// localStorage.setItem('token', data.access)

						setEmail("");
						setPassword1("");
						setPassword2("");

						Swal.fire({
							title: "Registration Successful",
							icon: "success",
							text: "Welcome to Zuitt!"
						})

						// navigate("/login");

					} else {

						Swal.fire({
							title: "Registration Failed",
							icon: "error",
							text: "Please, check your registration credentials and try again."
						})
					}
				})
				
			}
		})


	}




	useEffect(() => {
		if((firstname !== "" && lastname !== "" && mobileNo !== "" && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [firstname, lastname, email, mobileNo, password1, password2]);

	return(
		(user.id !== null) ?
			<Navigate to="/login" />
			:
			<Form onSubmit={e => registerUser(e)}>

					<Form.Group className="mb-3" controlId="firstname">
					  <Form.Label>First Name</Form.Label>
					  <Form.Control 
					  	type="firstname" 
					  	placeholder="Enter First Name"
					  	value={firstname}
					  	onChange={e => setfirstname(e.target.value)}
					  	required 
					  />
					  
					</Form.Group>

					<Form.Group className="mb-3" controlId="lastname">
					  <Form.Label>Last Name</Form.Label>
					  <Form.Control 
					  	type="lastname" 
					  	placeholder="Enter Last Name"
					  	value={lastname}
					  	onChange={e => setlastname(e.target.value)}
					  	required 
					  />
					  
					</Form.Group>


			      <Form.Group className="mb-3" controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
			        	type="email" 
			        	placeholder="Enter email"
			        	value={email}
			        	onChange={e => setEmail(e.target.value)}
			        	required 
			        />
			        
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="mobileNo">
			        <Form.Label>Mobile Number</Form.Label>
			        <Form.Control 
			        	type="mobileNo" 
			        	placeholder="Enter Mobile Number"
			        	value={mobileNo}
			        	onChange={e => setmobileNo(e.target.value)}
			        	required 
			        />
			        
			      </Form.Group>



			      <Form.Group className="mb-3" controlId="password1">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	placeholder="Password"
			        	value={password1}
			        	onChange={e => setPassword1(e.target.value)}
			        	required 
			        />
			      </Form.Group>
			    	
			      <Form.Group className="mb-3" controlId="password2">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	placeholder="Verify Password"
			        	value={password2}
			        	onChange={e => setPassword2(e.target.value)}
			        	required 
			        />
			      </Form.Group>

			    	{
			     		isActive ?
			     		<Button variant="primary" type="submit" id="submitBtn">
			     		  Submit
			     		</Button>
			     		:
			     		<Button variant="primary" type="submit" id="submitBtn" disabled>
			     		  Submit
			     		</Button>
			     	}
			    </Form>
	)
}